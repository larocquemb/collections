# Python tutorial for collections
# Paul La Rocque
# May 38, 2019

# there are four collection data types in the Python Programming language
#
# List is a collection which is ordered and changeable.  Duplicates allowed
# Tuple is a collection which is ordered and unchangeable.  Duplicates allowed
# Set is a collection which is unordered and unindexed.  No duplicates
# Dictionary is a collection which is unordered and indexed.  No duplicates

# lists use square brackets.  Use this for arrays
a1_list = [ 4, 120, "Julianne" ]
a2_list = [ 6, 150, "Daniel" ]
a3_list = [ 5, 130, "Chloe" ]
print (a1_list)
a1_elements = len(a1_list)
print ("Number of elements in a1_list", a1_elements)

# lists are mutable, optimized for speed
a1_list[0] = a2_list[0]
a1_list[1] = a2_list[1]
a1_list[2] = "Daniel"

print ("a1_list == a2_list: ", a1_list == a2_list)
print ("a1_list is a2_list: ", a1_list is a2_list)

# lists can be nested 
c_list = [ 
    a1_list,
    a2_list,
    a3_list
]
# single loop
for row in c_list:
    print (row)

# nested loop
for row in c_list:
    print ("==============")
    for col in row:
        print (col)

# tuples uses parenthesis.  
# Use this for SQL INSERT for good security no SQL header injections
# not optimized for speed
a_tuple = (4, 120, "Julianne" ) 
b_tuple = (4, 120, "Julianne" ) 
# to declare a tuple with 1 element add a comma at the end.
a_single_tuple = 4,
a_int = 4
a_string = "4"
print ("a_int data type", type(a_int))
print ("a_string data type", type(a_string))
print ("a_single_tuple data type", type(a_single_tuple))
print ("a_int == a_string", a_int == a_string)
print ("a_int == int(a_string)", a_int == int(a_string))
print ("a_int == a_tuple[0]", a_int == a_tuple[0])

for i in range(a1_elements):
    a1_list[i] = a_tuple[i]
c_list[0] = a1_list

print ("a1_list", a1_list, "id", id(a1_list))
print ("a_tuple", a_tuple, "id", id(a_tuple))
print ("c_list[0]", c_list[0], "id", id(c_list[0]))

if a1_list == c_list[0]:
    print("a1_list and c_list[0] refer to objects that are equal")
else:
    print("a1_list and c_list[0] refer to objects that are not equal")

if a1_list is c_list[0]:
    print("a1_list and c_list[0] are the same object")
else:
    print("a1_list and c_list[0] are not the same object")

if a_tuple == b_tuple:
    print("a_tuple and b_tuple refer to objects that are equal")
else:
    print("a_tuple and b_tuple refer to objects that are not equal")

if a_single_tuple is b_tuple:
    print("a_tuple and b_tuple are the same object")
else:
    print("a_tuple and b_tuple are however not the same object")

# comparison of different data types not explicit
if a1_list == a_tuple:
    print("a1_list and a_tuple refer to objects that are equal")
else:
    print("a1_list and a_tuple refer to objects that are not equal")

# comparison of different data types explicit casting
if a1_list == list(a_tuple):
    print("a1_list and list(a_tuple) refer to objects that are equal")
else:
    print("a1_list and list(a_tuple) refer to objects that are not equal")

# dictionary use curly braces.  Use this for key-value pairs, eg. JSON
a_dict = { 
    "age": 4, 
    "height": 120, 
    "name": "Julianne"
}

b_dict = { 
    "age": a3_list[0], 
    "height": a3_list[1],
    "name": a3_list[2]
}
# iterate over key and value in dictionary
for key in a_dict:
    print (key)
    print (key, a_dict[key])

# iterate values in dictionary
for value in a_dict.values():
    print (value)

# reference an element by its key
print (a_dict["age"])

nested_dict = {
    "La Rocque" : a_dict,
    "Tetreault" : b_dict
}

for family in nested_dict:
    print ("=== new family ===")
    print (family, nested_dict[family])
    for child in nested_dict[family]:
        print (child, nested_dict[family][child] )

# easier to read.  loop through key, value pairs in dictionary
for k, v in nested_dict.items():
    print ("{0} family {1}".format(k, v))  # new style for printing
    for child in v:
        print ("{0} is {1}".format(child, v[child]))